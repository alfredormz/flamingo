package flamingo_test

import (
	"testing"
	"time"

	"gitlab.com/alfredormz/flamingo"
)

func assertEqual(t *testing.T, got, want string) {
	t.Helper()
	if got != want {
		t.Errorf("got %q want %q", got, want)
	}
}

func TestTimeAgo(t *testing.T) {
	now := time.Now()

	t.Run("relative time in Spanish", func(t *testing.T) {
		testCases := []struct {
			time time.Time
			want string
		}{
			{now.Add(-5 * time.Second), "hace menos de un minuto"},
			{now.Add(-1 * time.Minute), "hace 1 minuto"},
			{now.Add(-5 * time.Minute), "hace 5 minutos"},
			{now.Add(-1 * time.Hour), "hace 1 hora"},
			{now.Add(-10 * time.Hour), "hace 10 horas"},
			{now.AddDate(0, 0, -1), "hace 1 día"},
			{now.AddDate(0, 0, -2), "hace 2 días"},
			{now.AddDate(0, 0, -7), "hace 1 semana"},
			{now.AddDate(0, 0, -14), "hace 2 semanas"},
			{now.AddDate(0, -1, 0), "hace 1 mes"},
			{now.AddDate(0, -5, 0), "hace 5 meses"},
			{now.AddDate(-1, 0, 0), "hace 1 año"},
			{now.AddDate(-5, 0, 0), "hace 5 años"},
		}

		for _, tc := range testCases {
			got := flamingo.TimeAgo(tc.time, flamingo.Spanish)
			assertEqual(t, got, tc.want)
		}
	})

	t.Run("relative time in English", func(t *testing.T) {
		testCases := []struct {
			time time.Time
			want string
		}{
			{now.Add(-5 * time.Second), "less than a minute ago"},
			{now.Add(-1 * time.Minute), "1 minute ago"},
			{now.Add(-5 * time.Minute), "5 minutes ago"},
			{now.Add(-1 * time.Hour), "1 hour ago"},
			{now.Add(-10 * time.Hour), "10 hours ago"},
			{now.AddDate(0, 0, -1), "1 day ago"},
			{now.AddDate(0, 0, -2), "2 days ago"},
			{now.AddDate(0, 0, -7), "1 week ago"},
			{now.AddDate(0, 0, -14), "2 weeks ago"},
			{now.AddDate(0, -1, 0), "1 month ago"},
			{now.AddDate(0, -5, 0), "5 months ago"},
			{now.AddDate(-1, 0, 0), "1 year ago"},
			{now.AddDate(-5, 0, 0), "5 years ago"},
		}

		for _, tc := range testCases {
			got := flamingo.TimeAgo(tc.time, flamingo.English)
			assertEqual(t, got, tc.want)
		}
	})
}
