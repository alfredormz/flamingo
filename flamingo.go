package flamingo

import (
	"fmt"
	"strings"
	"time"
)

const (
	Minute = time.Minute
	Hour   = time.Hour
	Day    = Hour * 24
	Week   = Day * 7
	Month  = Week * 4
	Year   = Month * 12
)

type FormatPeriod struct {
	One  string
	Many string
}

func (fp FormatPeriod) format(duration time.Duration) string {
	if int(duration) == 1 {
		return fp.One
	} else {
		return fmt.Sprintf(fp.Many, int(duration))
	}
}

func TimeAgo(t time.Time, config Config) string {
	diff := time.Since(t)

	past := false
	if diff >= 0 {
		past = true
	}

	if diff < 0 {
		diff = -diff
	}

	if past {
		return strings.Join(
			[]string{
				config.PastPrefix,
				config.getFormat(diff),
				config.PastSufix,
			}, "")
	} else {
		return strings.Join(
			[]string{
				config.FuturePrefix,
				config.getFormat(diff),
				config.FutureSufix,
			}, "")
	}
}

func (c Config) getFormat(diff time.Duration) string {
	switch {
	case diff >= Year:
		return c.FormatYears.format(diff / Year)
	case diff >= Month:
		return c.FormatMonths.format(diff / Month)
	case diff >= Week:
		return c.FormatWeeks.format(diff / Week)
	case diff >= Day:
		return c.FormatDays.format(diff / Day)
	case diff >= Hour:
		return c.FormatHours.format(diff / Hour)
	case diff >= Minute:
		return c.FormatMinutes.format(diff / Minute)
	default:
		return c.LessThanAMinute
	}
}

type Config struct {
	PastPrefix      string
	PastSufix       string
	FuturePrefix    string
	FutureSufix     string
	LessThanAMinute string
	FormatMinutes   FormatPeriod
	FormatHours     FormatPeriod
	FormatDays      FormatPeriod
	FormatWeeks     FormatPeriod
	FormatMonths    FormatPeriod
	FormatYears     FormatPeriod
}

var Spanish = Config{
	PastPrefix:      "hace ",
	FuturePrefix:    "en ",
	LessThanAMinute: "menos de un minuto",
	FormatMinutes:   FormatPeriod{One: "1 minuto", Many: "%d minutos"},
	FormatHours:     FormatPeriod{One: "1 hora", Many: "%d horas"},
	FormatDays:      FormatPeriod{One: "1 día", Many: "%d días"},
	FormatWeeks:     FormatPeriod{One: "1 semana", Many: "%d semanas"},
	FormatMonths:    FormatPeriod{One: "1 mes", Many: "%d meses"},
	FormatYears:     FormatPeriod{One: "1 año", Many: "%d años"},
}

var English = Config{
	PastSufix:       " ago",
	FuturePrefix:    "in ",
	LessThanAMinute: "less than a minute",
	FormatMinutes:   FormatPeriod{One: "1 minute", Many: "%d minutes"},
	FormatHours:     FormatPeriod{One: "1 hour", Many: "%d hours"},
	FormatDays:      FormatPeriod{One: "1 day", Many: "%d days"},
	FormatWeeks:     FormatPeriod{One: "1 week", Many: "%d weeks"},
	FormatMonths:    FormatPeriod{One: "1 month", Many: "%d months"},
	FormatYears:     FormatPeriod{One: "1 year", Many: "%d years"},
}
